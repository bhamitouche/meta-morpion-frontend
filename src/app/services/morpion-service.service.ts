import {Inject, Injectable} from '@angular/core';
import {IGame, INextRound, IPlayer, IRound} from "../entities/entities";
import {Observable} from "rxjs";
import {HttpClient, HttpParams} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class MorpionServiceService {

  constructor(@Inject('BASE_API_URL') private apiUrl, private httpClient: HttpClient) {
  }

  public initGame(starter: boolean, player: IPlayer, ipAddress?: string, vsAI?: boolean): Observable<IGame> {
    let httpParams = new HttpParams();
    httpParams = httpParams.set('starter', starter);
    httpParams = httpParams.set('vsAI', vsAI);
    if (!!ipAddress) {
      httpParams = httpParams.set('ip', ipAddress);
    }
    return this.httpClient.post<IGame>(this.apiUrl + '/morpion/init', player, {params: httpParams, observe: 'body'});
  }

  public play(round: IRound): Observable<INextRound> {
    return this.httpClient.post<INextRound>(this.apiUrl + '/morpion/play', round, {observe: 'body'});
  }

  public quitGame(): Observable<boolean> {
    return this.httpClient.delete<boolean>(this.apiUrl + '/morpion/quit', {observe: 'body'});
  }
}
