import { Injectable } from '@angular/core';
import {BehaviorSubject, Subject} from "rxjs";
import {Coordinates, IGame, INextRound, IPlayer} from "../entities/entities";
import * as StompJs from '@stomp/stompjs';

@Injectable({
  providedIn: 'root'
})
export class ConsumerService {
  public currentGame: Subject<IGame> = new Subject<IGame>();
  public playRound: Subject<INextRound> = new Subject<INextRound>();
  public quitGame: Subject<boolean> = new Subject<boolean>();
  public currentPlayer: BehaviorSubject<IPlayer> = new BehaviorSubject<IPlayer>(undefined);

  constructor() {}

  public consumeInitGame(clientStompJs: StompJs.Client): void {
    clientStompJs.subscribe('/init-game', (response) => {
      const game: IGame = JSON.parse(response.body);
      if (!game) {
        return;
      }
      this.currentGame.next(game);
      this.currentPlayer.next(game.currentPlayer);
    });
  }

  public consumePlayRound(clientStompJs: StompJs.Client): void {
    clientStompJs.subscribe('/play', (response) => {
      const nextRound: INextRound = JSON.parse(response.body);
      if (!nextRound) {
        return;
      }
      this.playRound.next(nextRound);
    });
  }

  public consumeQuitGame(clientStompJs: StompJs.Client): void {
    clientStompJs.subscribe('/quit', (response) => {
      const quit: boolean = JSON.parse(response.body);
      this.quitGame.next(quit);
    });
  }
}
