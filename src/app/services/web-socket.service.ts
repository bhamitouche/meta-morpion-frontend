import {APP_INITIALIZER, Injectable} from '@angular/core';
import * as SockJS from 'sockjs-client';
import * as StompJs from '@stomp/stompjs';
import {environment as env} from '../../environments/environment';
import {ConsumerService} from "./consumer.service";

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  private _clientStompJs: StompJs.Client;

  constructor(
    private consumer: ConsumerService
  ) {
  }

  public connect(): void {
    const webSocketEndPoint = '/api/ws';
    if (!this._clientStompJs || this._clientStompJs.connected) {
      // noinspection JSUnusedGlobalSymbols
      this._clientStompJs = new StompJs.Client({
        webSocketFactory: () => new SockJS(env.apiUrl + webSocketEndPoint),
      });
      this._clientStompJs.onConnect = () => this.onConnected();
      this._clientStompJs.onStompError = (frame) => {
        console.error(frame.headers['message']);
        console.error('Details:', frame.body);
      };
      this._clientStompJs.activate();
    }
  }

  private onConnected(): void {
    if (this._clientStompJs && this._clientStompJs.connected) {
      this._clientStompJs.publish({destination: '/swns/start'});
      this.consumer.consumeInitGame(this._clientStompJs);
      this.consumer.consumePlayRound(this._clientStompJs);
      this.consumer.consumeQuitGame(this._clientStompJs);
    }
  }

  public disconnect(): void {
    if (this._clientStompJs && this._clientStompJs.connected) {
      this._clientStompJs.publish({destination: '/swns/stop'});
    }
    if (this._clientStompJs && this._clientStompJs.connected) {
      this._clientStompJs.deactivate();
      this._clientStompJs = null;
    }
  }
}

export const WebSocketServiceProvider = [
  WebSocketService,
  {
    provide: APP_INITIALIZER,
    useFactory: (webSocketService: WebSocketService) => () => webSocketService.connect(),
    deps: [WebSocketService],
    multi: true
  }
];
