import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {GridComponent} from "./grid/grid.component";
import { ChildGridComponent } from './grid/child-grid/child-grid.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {WebSocketServiceProvider} from "./services/web-socket.service";
import {environment as env} from './../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    GridComponent,
    ChildGridComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [...WebSocketServiceProvider, {provide: 'BASE_API_URL', useValue: env.apiUrl},],
  bootstrap: [AppComponent]
})
export class AppModule { }
