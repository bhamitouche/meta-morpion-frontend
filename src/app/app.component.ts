import {Component, OnDestroy, OnInit} from '@angular/core';
import {IBoxValue, IGame, INextRound, IPlayer} from "./entities/entities";
import {MorpionServiceService} from "./services/morpion-service.service";
import {Subject, takeUntil} from "rxjs";
import {WebSocketService} from "./services/web-socket.service";
import {ConsumerService} from "./services/consumer.service";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  private unsubscribe: Subject<void> = new Subject();
  private playerName: string = 'Player 1';

  public isFinished: boolean = false;
  public winnerPlayer: IPlayer;
  public gameStarted: boolean = false;
  public chosenValue: IBoxValue = IBoxValue.NONE;
  public currentGame: IGame;
  public ipFormControl: FormControl = new FormControl();
  public isAI: boolean = false;
  public vsAI: boolean = false;

  constructor(private webSocketService: WebSocketService, private service: MorpionServiceService, private consumerService: ConsumerService) {
  }

  ngOnInit() {
    this.consumerService.currentGame.pipe(takeUntil(this.unsubscribe))
      .subscribe((currentGame) => {
        console.log(currentGame);
        this.currentGame = currentGame
      });

    this.consumerService.playRound.pipe(takeUntil(this.unsubscribe))
      .subscribe((nextRound: INextRound) => {
        this.isFinished = nextRound.finished;
        if (this.isFinished) {
          this.winnerPlayer = nextRound.player;
        }
      });

    this.consumerService.quitGame.pipe(takeUntil(this.unsubscribe))
      .subscribe((res) => {
        if (res) {
          this.consumerService.currentGame.next(undefined);
          this.isFinished = false;
          this.winnerPlayer = undefined;
          this.chosenValue = IBoxValue.NONE;
        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
    this.webSocketService.disconnect();
  }

  public onStartGame(): void {
    const player: IPlayer = {
      playerName: this.playerName,
      gameValue: this.chosenValue,
      ai: this.isAI
    };
    this.service.initGame(true, player, this.ipFormControl?.value, this.vsAI).pipe(takeUntil(this.unsubscribe))
      .subscribe(() => {
          console.log('Game Created');
        },
        (errors) => console.log(errors));
  }


  public setGameValue(xValue: string) {
    if (xValue === 'x_value') {
      this.chosenValue = IBoxValue.X_VALUE;
    } else {
      this.chosenValue = IBoxValue.O_VALUE;
    }
  }

  public onQuitGame(): void {
    this.service.quitGame().pipe(takeUntil(this.unsubscribe)).subscribe(() => {
      console.log("Quit Game");
    }, (errors) => console.log("Error quiting game!!"))
  }
}
