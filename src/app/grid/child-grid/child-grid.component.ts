import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit, ViewRef} from '@angular/core';
import {Coordinates, IBoxValue, IChildGrid, INextRound, IPlayer, IRound} from "../../entities/entities";
import {MorpionServiceService} from "../../services/morpion-service.service";
import {Subject, takeUntil} from "rxjs";
import {ConsumerService} from "../../services/consumer.service";

@Component({
  selector: 'app-child-grid',
  templateUrl: './child-grid.component.html',
  styleUrls: ['./child-grid.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChildGridComponent implements OnInit, OnDestroy {
  private unsubscribe: Subject<void> = new Subject();
  @Input() public childGrid: IChildGrid;
  @Input() public parentRow: number;
  @Input() public parentColumn: number;
  public boxValue: IBoxValue = IBoxValue.NONE;
  public isCurrentPlayerAI: boolean = false;
  public highlightedZone: Coordinates;

  constructor(private service: MorpionServiceService, private cdRef: ChangeDetectorRef, private consumer: ConsumerService) {
  }

  ngOnInit(): void {
    this.consumer.currentPlayer.pipe(takeUntil(this.unsubscribe))
      .subscribe((currentPlayer: IPlayer) => {
        this.boxValue = currentPlayer?.gameValue || IBoxValue.NONE;
        this.isCurrentPlayerAI = currentPlayer.ai ;
      });

    this.consumer.playRound.pipe(takeUntil(this.unsubscribe))
      .subscribe((nextRound: INextRound) => {
        if ((nextRound.nextColumn === null && nextRound.nextRow === null) || nextRound.finished) {
          this.highlightedZone = undefined;
        } else {
          this.highlightedZone = {column: nextRound.nextColumn, row: nextRound.nextRow};
        }
        if (this.isCurrentChildGrid(nextRound.playedRow, nextRound.playedColumn)) {
          if (nextRound.playedChildRow !== null && nextRound.playedChildColumn !== null) {
            this.setBoxValue(nextRound.playedChildRow, nextRound.playedChildColumn);
          }
          if (nextRound.lastChildFinished !== IBoxValue.NONE) {
            this.childGrid = {...this.childGrid, winnerValue: nextRound.lastChildFinished};
          }
        }
        this.boxValue = nextRound.player.gameValue;
        this.isCurrentPlayerAI = nextRound.player.ai;
        this.markForCheck();
      });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  public isCurrentChildGrid(parentRow: number, parentColumn: number): boolean {
    return parentColumn === this.parentColumn && parentRow === this.parentRow;
  }

  public getBoxValue(i: number, j: number): IBoxValue {
    return this.childGrid.boxes[i][j];
  }

  public setBoxValue(i: number, j: number): void {
    console.log(this.childGrid);
    this.childGrid.boxes[i][j] = this.boxValue;
  }

  public isBoxEmpty(i: number, j: number): boolean {
    return this.getBoxValue(i, j) === IBoxValue.NONE;
  }

  public playRound(i: number, j: number): void {
    if (i !== undefined && j !== undefined && this.isBoxEmpty(i, j) && (this.highlightedZone?.row === this.parentRow && this.highlightedZone?.column === this.parentColumn) || !this.highlightedZone) {
      const round: IRound = {
        row: this.parentRow,
        column: this.parentColumn,
        childRow: i,
        childColumn: j,
        value: this.boxValue
      };
      this.service.play(round).pipe(takeUntil(this.unsubscribe)).subscribe(() => {
        console.log("Played !!");
      }, (errors) => console.log("Error !!"))
    }
  }

  private markForCheck(): void {
    if (!!this.cdRef && !(this.cdRef as ViewRef).destroyed) {
      this.cdRef.markForCheck();
    }
  }
}
