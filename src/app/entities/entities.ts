export interface IFinishedGrid {
  winnerValue: IBoxValue;
}

export interface IGrid extends IFinishedGrid {
  childGrids: IChildGrid[][];
}

export interface IChildGrid extends IFinishedGrid {
  boxes: IBoxValue[][];
}

export interface IPlayer {
  gameValue: IBoxValue;
  playerName?: string;
  ai: boolean;
}

export interface IGame {
  player1: IPlayer;
  player2: IPlayer;
  currentPlayer: IPlayer;
  grid: IGrid;
  isFinished: boolean;
}

export interface IRound {
  row: number;
  column: number;
  childRow: number;
  childColumn: number;
  value: IBoxValue;
}

export interface INextRound {
  playedRow: number;
  playedColumn: number;
  playedChildRow: number;
  playedChildColumn
  nextRow: number;
  nextColumn: number;
  player: IPlayer;
  finished: boolean;
  lastChildFinished: IBoxValue;
}

export interface Coordinates {
  row: number;
  column: number;
  parentRow?: number;
  parentColumn?: number;
}

export enum IBoxValue {
  NONE = 'none',
  X_VALUE = 'x_value',
  O_VALUE = 'o_value'
}
